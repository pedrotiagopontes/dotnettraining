﻿using InsuranceApp.DAL;
using InsuranceApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsuranceApp.UnitTest
{
    [TestClass]
    public class TestGenericControllerWithDb:TestGenericController
    {
        
        [TestInitialize]
        public void Initialize()
        {
            _unitOfWork = new UnitOfWork<Manager>();
        }

        [TestMethod]
        public void GetById_Db_ShouldFind()
        {
            base.GetById_ShouldFind();
        }

        [TestMethod]
        public void GetById_Db_ShouldNotFind()
        {
            base.GetById_ShouldNotFind();
        }

        [TestMethod]
        public void Post_Db_ShouldAdd()
        {
            base.Post_ShouldAdd();
        }

        [TestMethod]
        public void Post_Db_ShouldNotAdd()
        {
            base.Post_ShouldNotAdd();
        }
    }
}
