﻿using System.Linq;
using System.Web.Http.Results;
using InsuranceApp.Controllers;
using InsuranceApp.Interface;
using InsuranceApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsuranceApp.UnitTest
{
    public abstract class TestGenericController
    {
        protected IUnitOfWork<Manager> _unitOfWork;
        protected static Manager MoqManager
        {
            get
            {
                var account = new Account()
                {
                    Id = 1,
                    Username = "Pontes",
                    Password = "blabla"
                };

                var manager = new Manager()
                {
                    Id = 1,
                    Address = "Rua Manuel Bandeira",
                    Identification = "Mr. Manager",
                    Account = account
                };
                return manager;
            }
        }

        public virtual void GetAll_ShouldFindAll()
        {
            var controller = new GenericController<Manager>(_unitOfWork);
            var expectedResultCount = 1;

            var result = controller.GetAll().ToList();
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedResultCount, result.Count);
        }

        public virtual void GetById_ShouldFind()
        {
            var controller = new GenericController<Manager>(_unitOfWork);
            var expected = 1;

            var result = controller.Get(1) as OkNegotiatedContentResult<Manager>; ;
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.Content.Id);
        }

        public virtual void GetById_ShouldNotFind()
        {
            var controller = new GenericController<Manager>(_unitOfWork);

            var result = controller.Get(99999);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        public virtual void Post_ShouldAdd()
        {
            var controller = new GenericController<Manager>(_unitOfWork);

            var manager = MoqManager;
            manager.Account.Username = "New Manager";

            var result = controller.Post(manager);
            Assert.IsInstanceOfType(result, typeof(CreatedAtRouteNegotiatedContentResult<Manager>));
        }


        public virtual void Post_ShouldNotAdd()
        {
            var controller = new GenericController<Manager>(_unitOfWork);
            var expectedResultCount = 1;

            controller.ModelState.AddModelError("Manager", "Invalid Manager");
            var result = controller.Post(new Manager());
            
            var resultCount = controller.GetAll().Count();
            Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));
        }
    }
}
