﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using InsuranceApp.Interface;

namespace InsuranceApp.UnitTest.MyMoq
{
    public class MoqUnitOfWork<TEntity>:IUnitOfWork<TEntity> where TEntity : class, IIdentifiable
    {
        private IRepository<TEntity> _repository;
        public IRepository<TEntity> Repository => _repository ?? (_repository = new MoqRespository<TEntity>());
        public void Dispose()
        {
        }
        
        public int SaveChanges()
        {
            return 0;
        }

        /// <summary>
        /// Dummy implementation of a in memory repository just for testing purposes.
        /// </summary>
        private class MoqRespository<TEntity> : IRepository<TEntity> where TEntity : class, IIdentifiable
        {
            private long counter = 0;
            private readonly Dictionary<long, TEntity> _inMemoryRepository = new Dictionary<long, TEntity>();
            public void Dispose()
            {}

            public TEntity Get(long key)
            {
                TEntity value;
                _inMemoryRepository.TryGetValue(key, out value);
                return value;
            }

            public IQueryable<TEntity> GetAll()
            {
                return _inMemoryRepository.Values.AsQueryable();
            }

            public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "")
            {
                var query = GetAll();
                if (filter != null)
                {
                    query = query.Where(filter);
                }


                query = ParseIncludePropertiesQuery(includeProperties, query);

                return query;
            }

            private static IQueryable<TEntity> ParseIncludePropertiesQuery(string includeProperties,
                IQueryable<TEntity> query)
            {
                foreach (var includeProperty in includeProperties.Split
                    (new char[] {','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
                return query;
            }

            public TEntity Add(TEntity item)
            {
                item.Id = ++counter;
                _inMemoryRepository.Add(item.Id, item);
                return item;
            }

            public TEntity Update(TEntity item)
            {
                _inMemoryRepository[item.Id] = item;
                return item;
            }

            public void Remove(long key)
            {
                _inMemoryRepository.Remove(key);
            }

            public void Remove(TEntity entityToDelete)
            {
                _inMemoryRepository.Remove(entityToDelete.Id);
            }
        }


    }
}
