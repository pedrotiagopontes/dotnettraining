﻿using System.Linq;
using System.Web.Http.Results;
using InsuranceApp.Controllers;
using InsuranceApp.Interface;
using InsuranceApp.Models;
using InsuranceApp.UnitTest.MyMoq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsuranceApp.UnitTest
{
    [TestClass]
    public class TestGenericControlerWithoutDb:TestGenericController
    {
        [TestInitialize]
        public void Initialize()
        {
            var manager = MoqManager;
            _unitOfWork = new MoqUnitOfWork<Manager>();
            _unitOfWork.Repository.Add(manager);
        }

        [TestMethod]
        public override void GetAll_ShouldFindAll()
        {
            base.GetAll_ShouldFindAll();
        }

        [TestMethod]
        public override void GetById_ShouldFind()
        {
            base.GetById_ShouldFind();
        }

        [TestMethod]
        public override void GetById_ShouldNotFind()
        {
            base.GetById_ShouldNotFind();
        }

        [TestMethod]
        public override void Post_ShouldAdd()
        {
            base.Post_ShouldAdd();
        }

        [TestMethod]
        public override void Post_ShouldNotAdd()
        {
            base.Post_ShouldNotAdd();
        }
    }
}
