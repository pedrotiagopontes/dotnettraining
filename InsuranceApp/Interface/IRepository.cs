﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace InsuranceApp.Interface
{
    public interface IRepository<TEntity>: IDisposable where TEntity : class, IIdentifiable
    {
        TEntity Get(long key);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Find(
            Expression<Func<TEntity, bool>> filter = null,
            string includeProperties = "");
        TEntity Add(TEntity item);
        TEntity Update(TEntity item);
        void Remove(long key);

        void Remove(TEntity entityToDelete);
    }
}