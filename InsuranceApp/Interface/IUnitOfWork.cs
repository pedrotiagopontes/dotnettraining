﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InsuranceApp.Models;

namespace InsuranceApp.Interface
{
    /// <summary>
    /// The unit of work class serves one purpose: to make sure that when you use multiple repositories, they share a single database context.
    /// For simplicity this unit of work only supports the same DbSets as the InsuranceAppContext
    /// </summary>
    public interface IUnitOfWork<TEntity>: IDisposable where TEntity : class, IIdentifiable
    {
        IRepository<TEntity> Repository { get; }
        int SaveChanges();

    }
}
