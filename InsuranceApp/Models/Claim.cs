﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace InsuranceApp.Models
{
    public class Claim:BaseEntity
    {
        public DateTime ClaimDate { get; set; }
        public decimal ReserveLine { get; set; }
        public string Description { get; set; }
        public ClaimStatus Status { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }

        //Navigation Properties
        [JsonIgnore]
        public virtual Policy Policy { get; set; }

        public Claim()
        {
            Payments = new List<Payment>();
        }
    }
}