﻿using Newtonsoft.Json;

namespace InsuranceApp.Models
{
    public class Payment:BaseEntity
    {
        public decimal Amount { get; set; }

        //Navigation Properties
        [JsonIgnore]
        public virtual Claim Claim { get; set; }
    }
}