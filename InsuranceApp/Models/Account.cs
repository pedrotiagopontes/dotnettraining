﻿using System.ComponentModel.DataAnnotations;

namespace InsuranceApp.Models
{
    public class Account : BaseEntity
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}