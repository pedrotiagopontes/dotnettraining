﻿namespace InsuranceApp.Models
{
    public enum ClaimStatus
    {
        Draft,
        Validated,
        Payment,
        Processed

    }
}