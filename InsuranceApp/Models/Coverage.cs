﻿using Newtonsoft.Json;

namespace InsuranceApp.Models
{
    public class Coverage:BaseEntity
    {
        public decimal Limit { get; set; }

        //Navigation Properties
        [JsonIgnore]
        public virtual Policy Policy { get; set; }
    }
}