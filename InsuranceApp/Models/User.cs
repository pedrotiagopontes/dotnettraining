﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsuranceApp.Models
{
    [Table("Users")]
    public class User: BaseEntity
    {
        public string Identification { get; set; }
        [Required]
        public string Address { get; set; }

    }
}