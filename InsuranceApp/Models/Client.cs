﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace InsuranceApp.Models
{
    [Table("Clients")]
    public class Client:User
    {
        //Navigation Properties
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Policy> Policies { get; set; }
        public Client()
        {
            Accounts = new List<Account>();
            Policies = new List<Policy>();
        }
        
    }
}