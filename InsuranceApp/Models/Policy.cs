﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InsuranceApp.Interface;
using Newtonsoft.Json;

namespace InsuranceApp.Models
{
    public class Policy: BaseEntity
    {
        [Required]
        public int PolicyNo { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        public Coverable Coverable { get; set; }

        //Navigation Properties
        public virtual ICollection<Coverage> Coverages { get; set; }
        public virtual ICollection<Claim> Claims { get; set; }
        [JsonIgnore]
        public virtual Client Client {get; set; }

        public Policy()
        {
            Coverages = new List<Coverage>();
            Claims = new List<Claim>();
        }

        
    }
}