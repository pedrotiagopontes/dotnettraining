﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsuranceApp.Models
{
    [Table("Managers")]
    public class Manager:User
    {
        [Required]
        public Account Account { get; set; }
    }
}