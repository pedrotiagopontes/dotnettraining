﻿using System;
using System.Data.Entity;
using InsuranceApp.Interface;

namespace InsuranceApp.DAL
{
    public class UnitOfWork<TEntity>: IUnitOfWork<TEntity> where TEntity : class, IIdentifiable
    {
        //Hard-coded depedency on the InsuranceAppContext. For a better solution it should be injected.
        private readonly DbContext _dbContext = new InsuranceAppContext();
        private IRepository<TEntity> _repository;

        public IRepository<TEntity> Repository =>_repository ?? (_repository = new GenericRepository<TEntity>(_dbContext));

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        #region Generic Dispose implementation
        /* Generic Dispose implementation from: 
         * https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application
         */
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}