﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using InsuranceApp.Interface;

namespace InsuranceApp.DAL
{
    /// <summary>
    /// Creates a generic repository from CRUD operations
    /// </summary>
    /// <typeparam name="TEntity">Type of the Repository</typeparam>
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class, IIdentifiable
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public virtual TEntity Get(long key)
        {
            return _dbSet.FirstOrDefault((u) => u.Id == key);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }


            query = ParseIncludePropertiesQuery(includeProperties, query);

            return query;
        }

        private static IQueryable<TEntity> ParseIncludePropertiesQuery(string includeProperties, IQueryable<TEntity> query)
        {
            foreach (var includeProperty in includeProperties.Split
                (new char[] {','}, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public virtual TEntity Add(TEntity item)
        {
            var newItem = _dbSet.Add(item);
            return newItem;
        }

        public virtual TEntity Update(TEntity item)
        {
            _dbSet.AddOrUpdate(i => item, item);
            return item;
        }

        public virtual void Remove(long key)
        {
            var item = Get(key);
            _dbSet.Remove(item);
        }

        public void Remove(TEntity entityToDelete)
        {
            if (_dbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }

        #region Generic Dispose implementation
        /* Generic Dispose implementation from: 
         * https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application
         */
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
