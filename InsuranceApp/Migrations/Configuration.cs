using InsuranceApp.DAL;
using InsuranceApp.Models;

namespace InsuranceApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InsuranceAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(InsuranceAppContext context)
        {
            var account = new Account()
            {
                Id = 1,
                Username = "Pontes",
                Password = "blabla"
            };

            var manager = new Manager()
            {
                Id = 1,
                Address = "Rua Manuel Bandeira",
                Identification = "Mr. Manager",
                Account = account
            };

            context.Managers.AddOrUpdate(x => x.Id, manager);

            //Client
            var client = new Client()
            {
                Id = 2,
                Address = "Another address",
                Identification = "Mr. Client"
            };
            
            var payment = new Payment()
            {
                Amount = 1100
            };
            var claim = new Claim()
            {
                ClaimDate = DateTime.Now,
                Description = "The house is on fire!!!",
                ReserveLine = 1000,
                Status = ClaimStatus.Draft,
            };
            var coverage = new Coverage()
            {
                Limit = 1000
            };

            var policy = new Policy()
            {
                PolicyNo = 999,
                StartDate = DateTime.Now,
                EndDate = DateTime.MaxValue,
                Coverable = new Coverable() { Details = "House" }
            };

            claim.Payments.Add(payment);
            policy.Claims.Add(claim);
            policy.Coverages.Add(coverage);
            
            client.Accounts.Add(new Account() { Id = 2, Username = "Client1", Password = "blabla2"});
            client.Policies.Add(policy);

            context.Clients.AddOrUpdate(x => x.Id, client);
        }
    }
}
