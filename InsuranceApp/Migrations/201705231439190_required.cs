namespace InsuranceApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class required : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Managers", "Account_Id", "dbo.Accounts");
            DropIndex("dbo.Managers", new[] { "Account_Id" });
            AlterColumn("dbo.Accounts", "Username", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Managers", "Account_Id", c => c.Long(nullable: false));
            CreateIndex("dbo.Managers", "Account_Id");
            AddForeignKey("dbo.Managers", "Account_Id", "dbo.Accounts", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Managers", "Account_Id", "dbo.Accounts");
            DropIndex("dbo.Managers", new[] { "Account_Id" });
            AlterColumn("dbo.Managers", "Account_Id", c => c.Long());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Accounts", "Username", c => c.String());
            CreateIndex("dbo.Managers", "Account_Id");
            AddForeignKey("dbo.Managers", "Account_Id", "dbo.Accounts", "Id");
        }
    }
}
