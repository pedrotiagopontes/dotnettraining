namespace InsuranceApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Client_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id)
                .Index(t => t.Client_Id);
            
            CreateTable(
                "dbo.Claims",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClaimDate = c.DateTime(nullable: false),
                        ReserveLine = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                        Policy_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Policies", t => t.Policy_Id)
                .Index(t => t.Policy_Id);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Claim_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Claims", t => t.Claim_Id)
                .Index(t => t.Claim_Id);
            
            CreateTable(
                "dbo.Policies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PolicyNo = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Client_Id = c.Long(),
                        Coverable_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id)
                .ForeignKey("dbo.Coverables", t => t.Coverable_Id)
                .Index(t => t.Client_Id)
                .Index(t => t.Coverable_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Identification = c.String(),
                        Address = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Coverables",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Coverages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Limit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Policy_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Policies", t => t.Policy_Id)
                .Index(t => t.Policy_Id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Managers",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Account_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.Id)
                .Index(t => t.Account_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Managers", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.Managers", "Id", "dbo.Users");
            DropForeignKey("dbo.Clients", "Id", "dbo.Users");
            DropForeignKey("dbo.Coverages", "Policy_Id", "dbo.Policies");
            DropForeignKey("dbo.Policies", "Coverable_Id", "dbo.Coverables");
            DropForeignKey("dbo.Policies", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.Accounts", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.Claims", "Policy_Id", "dbo.Policies");
            DropForeignKey("dbo.Payments", "Claim_Id", "dbo.Claims");
            DropIndex("dbo.Managers", new[] { "Account_Id" });
            DropIndex("dbo.Managers", new[] { "Id" });
            DropIndex("dbo.Clients", new[] { "Id" });
            DropIndex("dbo.Coverages", new[] { "Policy_Id" });
            DropIndex("dbo.Policies", new[] { "Coverable_Id" });
            DropIndex("dbo.Policies", new[] { "Client_Id" });
            DropIndex("dbo.Payments", new[] { "Claim_Id" });
            DropIndex("dbo.Claims", new[] { "Policy_Id" });
            DropIndex("dbo.Accounts", new[] { "Client_Id" });
            DropTable("dbo.Managers");
            DropTable("dbo.Clients");
            DropTable("dbo.Coverages");
            DropTable("dbo.Coverables");
            DropTable("dbo.Users");
            DropTable("dbo.Policies");
            DropTable("dbo.Payments");
            DropTable("dbo.Claims");
            DropTable("dbo.Accounts");
        }
    }
}
