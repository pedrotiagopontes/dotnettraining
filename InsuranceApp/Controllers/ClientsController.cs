﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class ClientsController : GenericController<Client>
    {
        public ClientsController(IUnitOfWork<Client> unitOfWork) : base(unitOfWork)
        {

        }
    }
}