﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class UsersController : GenericController<User>
    {
        public UsersController(IUnitOfWork<User> unitOfWork) : base(unitOfWork)
        {

        }
    }
}