﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class ClaimsController : GenericController<Claim>
    {
        public ClaimsController(IUnitOfWork<Claim> unitOfWork) : base(unitOfWork)
        {

        }
    }
}