﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class PoliciesController : GenericController<Policy>
    {
        public PoliciesController(IUnitOfWork<Policy> unitOfWork) : base(unitOfWork)
        {

        }
    }
}