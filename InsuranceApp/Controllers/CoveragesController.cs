﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class CoveragesController : GenericController<Coverage>
    {
        public CoveragesController(IUnitOfWork<Coverage> unitOfWork) : base(unitOfWork)
        {

        }
    }
}