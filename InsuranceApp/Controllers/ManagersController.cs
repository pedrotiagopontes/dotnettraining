﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class ManagersController:GenericController<Manager>
    {
        public ManagersController(IUnitOfWork<Manager> unitOfWork) : base(unitOfWork)
        {

        }
    }
}