﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class AccountsController : GenericController<Account>
    {
        public AccountsController(IUnitOfWork<Account> unitOfWork) : base(unitOfWork)
        {

        }
    }
}