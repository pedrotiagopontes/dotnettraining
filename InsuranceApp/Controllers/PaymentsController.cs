﻿using InsuranceApp.Interface;
using InsuranceApp.Models;

namespace InsuranceApp.Controllers
{
    public class PaymentsController : GenericController<Payment>
    {
        public PaymentsController(IUnitOfWork<Payment> unitOfWork) : base(unitOfWork)
        {

        }
    }
}