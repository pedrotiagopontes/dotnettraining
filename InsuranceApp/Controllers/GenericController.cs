﻿using System.Linq;
using System.Net;
using System.Web.Http;
using InsuranceApp.Filters;
using InsuranceApp.Interface;

namespace InsuranceApp.Controllers
{
    public class GenericController<T> : ApiController where T : class, IIdentifiable
    {
        protected readonly IUnitOfWork<T> UnitOfWork;

        public GenericController(IUnitOfWork<T> unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        // GET: api/T
        public virtual IQueryable<T> GetAll()
        {
            return UnitOfWork.Repository.GetAll();
        }

        // GET: api/T/5
        public virtual IHttpActionResult Get(long id)
        {
            T item = UnitOfWork.Repository.Get(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/T/5
        public virtual IHttpActionResult Put(long id, T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.Id)
            {
                return BadRequest();
            }

            UnitOfWork.Repository.Update(item);
            UnitOfWork.SaveChanges();
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/T
        public virtual IHttpActionResult Post(T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UnitOfWork.Repository.Add(item);
            UnitOfWork.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = item.Id }, item);
        }

        // DELETE: api/T/5
        public virtual IHttpActionResult Delete(long id)
        {
            UnitOfWork.Repository.Remove(id);
            UnitOfWork.SaveChanges();

            return Ok(id);
        }

        protected override void Dispose(bool disposing)
        {
            UnitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}