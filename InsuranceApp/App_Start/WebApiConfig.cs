﻿using System.Web.Http;
using InsuranceApp.DAL;
using InsuranceApp.Filters;
using InsuranceApp.Interface;
using InsuranceApp.Models;
using Microsoft.Practices.Unity;

namespace InsuranceApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            ConfigureContainer(config);

            // Web API Global Filters
            config.Filters.Add(new ValidateModelAttribute());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void ConfigureContainer(HttpConfiguration config)
        {
            var container = new UnityContainer();

            RegisterUnitOfWork<Account>(container);
            RegisterUnitOfWork<Manager>(container);
            RegisterUnitOfWork<Client>(container);

            RegisterUnitOfWork<Policy>(container);
            RegisterUnitOfWork<Coverable>(container);
            RegisterUnitOfWork<Coverage>(container);

            RegisterUnitOfWork<Claim>(container);
            RegisterUnitOfWork<Payment>(container);

            config.DependencyResolver = new UnityResolver(container);
        }

        private static void RegisterUnitOfWork<TEntity>(UnityContainer container) where TEntity : class, IIdentifiable
        {
            container.RegisterType<IUnitOfWork<TEntity>, UnitOfWork<TEntity>>();
        }
    }
}
