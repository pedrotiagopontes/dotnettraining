# README #

InsuranceApp is a solution composed by a set of basic REST services for the CRUD operations of the typical entities found in an Insurance application (Clients, Managers, Policies, Claims).

The purpose of this app was not to develop a full solution, but to act as a sand-box for the following technologies and patterns:

* ASP.NET Web API 2
* Unit of Work and Repository Patterns
* Unit Testing with Moq and Db

### End-Points and operations ###

* [Swagger endpoints](http://localhost:50260/swagger/ui/index)

### Set-up ###

* Database: Database can be re-created using code-first migrations (Use "Update-database" command in the Package Manager Console)

### Who do I talk to? ###

* [Pedro Pontes](mailto:"pepontes@deloitte.pt")